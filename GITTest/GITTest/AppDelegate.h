//
//  AppDelegate.h
//  GITTest
//
//  Created by Oscar Gomez on 1/9/13.
//  Copyright (c) 2013 Oscar Gomez. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
